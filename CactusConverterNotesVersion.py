#!/usr/bin/env python3

import errno
import subprocess
import mmap
import h5py
import numpy as np
import re
import sys
import os
import datetime
import romspline
try:
    import ConfigParser  # renamed to configparser in Python3
except ImportError:
    import configparser as ConfigParser

class CactusConverter:
    ''' A class that converts output format from cactus to LIGO format. Internally it uses Mathematica script (with SimulationTools package) to generate metadata and waveform (hdf5), and then it uses python script to parse and combine metadata with waveform in LIGO compatible format.

    @author Wei Ren
    @param  simulationPath - path to simulations being converted
            outputPath - path to output directory
            simulations - list of simulation names
            nrGroup - Name of the group (default to "NCSA").
    '''
    def __init__(self, simulationPath, outputPath, simulationList, nrGroup = "NCSA"):
        self.simulationPath = simulationPath
        self.outputPath = outputPath
        self.simulationList = simulationList
        self.nrGroup = nrGroup

    def toSXS(self):
        '''
        Export Cactus BBH simulations to SXS format.

        @return - None.
        @output - output files in the outputPath.
        '''
        procList = []
        for sim in self.simulationList:
            simPath = os.path.join(self.simulationPath, sim)
            simfactoryPath = os.path.join(simPath, "SIMFACTORY")
            if not os.path.isdir(simfactoryPath):
                os.mkdir(simfactoryPath, 0o755)
            proc = subprocess.Popen(["wolfram", "-nohup", "-script", "CactusToSXS.m", sim, self.outputPath, self.simulationPath])
            #CactusToSXS produces the same that power does but with extra things. mathematica output is equivalent to POWER, produces some rh/asymptotic file
            #metadata is also produced by mathematica file, POWER does not produce this but this is where Sarah's script comes in hook
            procList.append(proc)
        # Simple barrier
        for proc in procList:
            proc.wait()
        self.__hackEccentricity()
        print("Export to SXS format done!")

    def toLIGO(self, exportSXS=True):
        '''
        Export Cactus BBH simulations to LIGO format. A boolean flag is also provided. It's useful when the user already exported SXS format, but the user must ensure SXS output files are in the outputPath otherwise the function will fail.

        @param exportSXS - a boolean flag to export SXS (or not).
        @return - None.
        @output - output files in outputPath.
        '''
        if exportSXS:
            self.toSXS()
        for sim in self.simulationList:
            self.__toLIGO(sim)

    def exportStrainData(self, r_0 = 500, omega = 0.01):
        '''
        Export strain information (phase, frequency, amplitude) in *.dat files.

        @param r_0 - default to 500.
        @param omega - default to 0.01.
        @return - None.
        @output - output files in outputPath.
        '''
        procList = []
        for sim in self.simulationList:
            proc = subprocess.Popen(["wolfram", "-nohup", "-script", "exportStrainData.m", str(omega), str(r_0), sim, self.outputPath, self.simulationPath])
            procList.append(proc)
        for proc in procList:
            proc.wait()

    def __toLIGO(self, simName):
        '''
        Convert to LIGO format.

        @param - Name of the one simulation.
        @return - None.
        @output - output hdf5 file with simulation information in outputPath.
        '''
        def getfloats(self, section, option):
            # helper function to parse floats
            return np.array([float(elem) for elem in re.split(", *", self.get(section, option))])
        # see https://stackoverflow.com/a/2982
        simPath = os.path.join(self.outputPath, simName)
        metafile = os.path.join(simPath, "metadata.txt") 
        # hook join metadata files produced by MMA code "relaxed-eccentricity" is line where eccentricity measurement sarah's code will eventually calculate
        #Sarah's code will produce one more line then this metadata.txt file, replaces that portion of the MMA code. hook
        SXSfile = os.path.join(simPath, "rhOverM_Asymptotic_GeometricUnits.h5") #hook (could replace earlier with toSXS or here)
        if not os.path.isfile(metafile):
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), metafile)
        ConfigParser.RawConfigParser.getfloats = getfloats
        metadata = ConfigParser.RawConfigParser()
        metadata.read(metafile)
        outputLIGO = os.path.join(simPath, simName+".h5")

        #hook. attributes are all the meta-data attributes 
        with h5py.File(outputLIGO, "w") as output:
            attr = output.attrs
            attr['Format'] = 1
            attr['type'] = 'NRinjection'
            # TODO: determine type (and name) based on information in metadata file
            attr['name'] = '{group}:BBH:{simulation}'.format(
                group=self.nrGroup, simulation=simName)
            attr['alternative-names'] = '{group}:Binary Black Hole:{simulation}'.format(
                group=self.nrGroup, simulation=simName)
            attr['NR-group'] = self.nrGroup
            attr['NR-code'] = 'Einstein Toolkit'
            attr['modification-date'] = datetime.date.today().strftime('%Y/%m/%d')
            attr['point-of-contact-email'] = metadata.get("metadata", "point-of-contact-email")
            # TODO: check simulation-type based on spin values
            attr['simulation-type'] = 'non-spinning'
            attr['INSPIRE-bibtex-keys'] = ''
            attr['license'] = 'LVC-internal'  # TODO: choose one.
            attr['Lmax'] = 2
            attr['NR-techniques'] = ''  # TODO: refer to the updated document
            attr['files-in-error-series'] = ''
            attr['comparable-simulation'] = ''
            attr['production-run'] = 1
            attr['object1'] = 'BH'
            attr['object2'] = 'BH'
            attr['PN_approximant'] = 'None'

            mass1 = metadata.getfloat('metadata', 'relaxed-mass1')
            attr['mass1'] = mass1
            mass2 = metadata.getfloat('metadata', 'relaxed-mass2')
            attr['mass2'] = mass2
            spin1 = metadata.getfloats('metadata', 'relaxed-spin1')
            attr['spin1x'] = spin1[0]n
            attr['spin1y'] = spin1[1]
            attr['spin1z'] = spin1[2]
            spin2 = metadata.getfloats('metadata', 'relaxed-spin2')
            attr['spin2x'] = spin2[0]
            attr['spin2y'] = spin2[1]
            attr['spin2z'] = spin2[2]
            position1 = metadata.getfloats('metadata', 'relaxed-position1')
            position2 = metadata.getfloats('metadata', 'relaxed-position2')
            n = position1 - position2  # from 2 to 1
            nhat = n / np.linalg.norm(n)
            attr['nhatx'] = nhat[0]
            attr['nhaty'] = nhat[1]
            attr['nhatz'] = nhat[2]
            attr['eccentricity'] = metadata.getfloat(
               'metadata', 'relaxed-eccentricity') #hook
            #hook. relaxed-eccentricity and mean-anomaly will both come from Sarah's code 
           attr['mean_anomaly'] = metadata.getfloat(
               'metadata', 'relaxed-mean-anomaly')
            hard-codes z axis as orbital axis
           orbit_f = metadata.getfloats('metadata', 'relaxed-orbital-frequency')
            attr['Omega'] = orbit_f[2]
            f_lower_at_1MSUN = orbit_f[2] / (np.pi * 4.92549095e-6)
           attr['f_lower_at_1MSUN'] = f_lower_at_1MSUN
            attr['eta'] = mass1 * mass2 / (mass1 + mass2)**2
            attr['LNhatx'] = 0.
            attr['LNhaty'] = 0.
            NOTE: check based on spin vector instead of hardcode.
            attr['LNhatz'] = 1.

            
            #hook. For all the other funcitons/classes that involve metadata have to figure out how MMA computed them 
            #and write equivalent python code 
            #hook whenever it says relaxed it just means the measurement taken at that time 
            #hook probably hardcoded. single digit probably hardcoded, decimal maybe not 
            relaxed_measurement_time = metadata.getfloat("metadata", "relaxed-measurement-time")

            output.create_group('auxiliary-info')

            with h5py.File(SXSfile, 'r') as fSXS:
                extrapolatedGroups = list(filter(lambda n: re.match(
                    "Extrapolated_N[0-9]+\\.dir", n), fSXS.keys()))
                print(type(extrapolatedGroups))
                if(len(extrapolatedGroups) != 1):
                    raise IndexError(
                        "Could not identify unique data group in %s" % ",".join(fSXS.keys()))
                extrapolatedGroup = extrapolatedGroups[0]

                ds_regexp = "Y_l([0-9]+)_m(-?[0-9]+)\\.dat"
                modes = [re.match(ds_regexp,ds).groups()[0:2]
                         for ds in fSXS[extrapolatedGroup] if re.match(ds_regexp, ds)]

                for (l,m) in modes:
                    print("Exporting l=%s, m=%s" % (l, m))
                    ampGroup = 'amp_l%s_m%s' % (l, m)
                    phaseGroup = 'phase_l%s_m%s' % (l, m)
                    output.create_group(ampGroup)
                    output.create_group(phaseGroup)

                    strain = fSXS[extrapolatedGroup + "/Y_l%s_m%s.dat" % (l, m)][:]
                    # we need to enforce that the first data point is actually at t=relaxed_measurement_time
                    # TODO: if it's not there, use a spline interpolation to create it?
                    if not (relaxed_measurement_time >= strain[0, 0] and relaxed_measurement_time <= strain[-1, 0]) :
                        raise IndexError(
                            "relaxed_measurement_time %g is not a node of the time series for strain" %
                                relaxed_measurement_time)

                    mask = strain[:, 0] > relaxed_measurement_time # Truncate data before relaxed_measurement_time
                    strain = strain[mask, :]
                    t = strain[:, 0]
                    amplitude = np.sqrt(strain[:, 1]**2 + strain[:, 2]**2)
                    phase = np.unwrap(np.arctan2(strain[:, 2], strain[:, 1]))

                    spline = romspline.ReducedOrderSpline(t, amplitude, verbose=False)
                    spline.write(output[ampGroup])

                    spline = romspline.ReducedOrderSpline(t, phase, verbose=False)
                    spline.write(output[phaseGroup])

                output.create_dataset('NRtimes', data=t)

    def simulationPath():
        doc = "The simulationPath property."
        def fget(self):
            return self._simulationPath
        def fset(self, value):
            if not os.path.isdir(value):
                raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), value)
            self._simulationPath = value
        def fdel(self):
            del self._simulationPath
        return locals()
    simulationPath = property(**simulationPath())

    def outputPath():
        doc = "The outputPath property."
        def fget(self):
            return self._outputPath
        def fset(self, value):
            if not os.path.isdir(value):
                os.mkdir(value, 0o755)
            self._outputPath = value
        def fdel(self):
            del self._outputPath
        return locals()
    outputPath = property(**outputPath())

    def simulationList():
        doc = "The simulationList property."
        def fget(self):
            return self._simulationList
        def fset(self, value):
            try:
                if isinstance(value, str):
                    raise TypeError("'simulationList' should be a **list** of simulation names (strings).")
                for elem in value:
                    sim = os.path.join(self.simulationPath, elem)
                    if not os.path.isdir(sim):
                        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), sim)
            except Exception as e:
                print("'simulationList' is not a list of simulation names or it contains simulations that does not exists in 'simulationPath'.\n")
                raise
            self._simulationList = value
        def fdel(self):
            del self._simulationList
        return locals()
    simulationList = property(**simulationList())

    def __hackEccentricity(self):
        # Eccentricity is complex in the output metadata.txt for some reason.
        # A hacky function to keep the real part.
        for sim in self.simulationList:
            simOutputPath = os.path.join(self.outputPath, sim)
            metafile = os.path.join(simOutputPath, "metadata.txt")
            with open(metafile, "r") as f:
                tempString = f.read()
                newString = tempString
                match = re.search("(^.*?)(Complex[(])([-+]?\d*\.\d+|\d+)([,])([-+]?\d*\.\d+|\d+)([)])(.*?)$", tempString, re.DOTALL)
                if match:
                    newString = match.group(1) + match.group(3) + match.group(7)
            with open(metafile, "w") as f:
                f.write(newString)
# Testing
if __name__ == '__main__':
    a = CactusConverter("/home/wei/numericalRelativity/simulations", "/home/wei/numericalRelativity/temp", ["E0017_N32", "E0017_N36", "E0017_N40"], "NCSA")
    print(a.simulationPath)
    print(a.outputPath)
    print(a.simulationList)
