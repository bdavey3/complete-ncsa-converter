import numpy as np
import re
import glob
import scipy
import matplotlib as mpl
import matplotlib.pyplot as plt
%matplotlib inline

def ParseASCIIHeader(filename):
    columns = {}
    with open(filename, "r") as fh:
        for line in fh:
            line = line.rstrip("\n")
            mComment = re.match(r"^#\s*", line)
            mBlank = re.match(r"^\s*$", line)
            if(mComment):
                m = re.match(r"^#\s*([^:]*):", line)
                if(m and (m.group(1) == "column format" or m.group(1) == "data columns")):
                    fields = line.split()[3:]
                    for field in fields:
                        (colnum, colname) = field.split(":")
                        columns[colname] = int(colnum) - 1
                m = re.match(r"^#\s*column\s+([0-9]+)\s*=\s*(.*)", line)
                if(m):
                    (colnum, colname) = (m.group(1), m.group(2))
                    columns[colname] = int(colnum) - 1
            if(not mComment and not mBlank):
                break
    if not columns: ValueError("Could not parse header in %s" % filename)
    return columns


def ReadColumnFile(runName, fileName, columns=None):
    """Read a ASCII file with columns, accept a list of columns to read
    as either column numbers counting from 0 or column names which we try
    and parse out of the header"""
    fileGlob = "%s/output-????/%s/%s" % (runName, runName, fileName)
    
    firstFile = glob.glob(fileGlob)[0]
    dataColumns = ParseASCIIHeader(firstFile)
    
    data = loadASCIISeries(fileGlob)
    
    if columns is None:
        return data
    else:
        # check if columns are integers or strings
        try:
            int(columns[0])
            is_String = False
        except ValueError:
            is_String = True
        if(is_String):
            cols = [dataColumns[i] for i in columns]
        else:
            cols = columns
        return data[cols,:]
    ValueError("Internal Error")

def ReadAHMass(runName, hn):
    return ReadColumnFile(runName, "BH_diagnostics.ah%d.gp" % hn, [1, 26])

def ReadIsolatedHorizonSpin(runName, hn):
    # TODO: make ReadColumFile accept a pattern
    # FIXME: need to pass a list of columns to read to ReadColumnFile
    return ReadColumnFile(runName, "quasilocalmeasures-qlm_scalars..asc",
                          ["time", "qlm_coordspinx[%d]" % hn, "qlm_coordspiny[%d]" % hn,
                           "qlm_coordspinz[%d]" % hn])

def ChristodoulouMass(run, ahn, ihn):
    mIrr = ReadAHMass(run, ahn)
    Svec = ReadIsolatedHorizonSpin(run,ihn)
    S = np.empty(shape=(2, Svec.shape[1]), dtype=Svec.dtype)
    S[0,:] = Svec[0,:]
    S[1,:] = np.sqrt(Svec[1,:]**2+Svec[2,:]**2+Svec[3,:]**2)
    
    (mIrr, S) = ResampleToSmallestTimestep([mIrr, S])
    
    mChristodoulou = np.empty(shape=(2, Svec.shape[1]), dtype=S.dtype)
    mChristodoulou[0,:] = S[0, :]
    mChristodoulou[1,:] = np.sqrt(mIrr**2 + S**2/(4*mIrr**2))


